
Code from the [mandelbrot-numerics library](https://code.mathr.co.uk/mandelbrot-numerics) by [Claude Heiland-Allen](http://mathr.co.uk/)
# description	

numerical algorithms related to the Mandelbrot set   

# c programs 

## m-interior

code : [m-interior.c](m-interior.c)  
  
description:
* [wikibooks](https://en.wikibooks.org/wiki/Fractals/mandelbrot-numerics#m-interior)







# License

This project is licensed under the  Creative Commons Attribution-ShareAlike 4.0 International License - see the [LICENSE.md](LICENSE.md) file for details  

# technical notes
GitLab uses:
* the Redcarpet Ruby library for [Markdown processing](https://gitlab.com/gitlab-org/gitlab-ce/blob/master/doc/user/markdown.md)
* KaTeX to render [math written with the LaTeX syntax](https://gitlab.com/gitlab-org/gitlab-ce/blob/master/doc/user/markdown.md), but [only subset](https://khan.github.io/KaTeX/function-support.html)




## Git
```
cd existing_folder
git init
git remote add origin git@gitlab.com:adammajewski/mandelbrot-numerics.git
git add .
git commit -m "Initial commit"
git push -u origin master
```
